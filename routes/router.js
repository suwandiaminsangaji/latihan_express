import express from "express";
import { checkHealth } from '../controllers/checkHealthController.js';
import { checkAuthKey } from "../middleware/checkKey.js";


const router = express.Router()
router.get('/check-health', checkHealth)
router.get('/check-auth-key', checkAuthKey)

export default router